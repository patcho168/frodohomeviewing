//
//  CameraViewController.swift
//  HomeViewing
//
//  Created by Jason on 5/8/17.
//  Copyright © 2017 TinyWhale. All rights reserved.
//

import UIKit
import ARKit
import SnapKit

class CameraViewController: UIViewController {
    var sceneView:ARSCNView?
    var planes:[String:Plane]?
    var arConfig:ARWorldTrackingSessionConfiguration?
    var allModel:AllModel?
    var statusDot:UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        
        self.setupScene()
        
        if let sceneView = self.sceneView {
            self.view.addSubview(sceneView)
            sceneView.snp.makeConstraints({ (make) in
                make.size.equalTo(self.view)
            })
        }
        
        self.setupPhysics()
        self.setupRecognizer()
        
        self.statusDot = UIView(frame: .zero)
        
        if let sceneView = self.sceneView,
            let statusDot = self.statusDot {
            self.view.addSubview(statusDot)
            statusDot.snp.makeConstraints { (make) in
                make.bottom.equalTo(sceneView).offset(-10);
                make.right.equalTo(sceneView).offset(-10);
                make.width.equalTo(8)
                make.height.equalTo(8)
            }
            statusDot.backgroundColor = UIColor.red
            statusDot.layer.cornerRadius = 4
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.sceneView?.session.run(self.arConfig!, options: [.resetTracking, .removeExistingAnchors])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.sceneView?.session.pause()
        if let allModel = self.allModel {
            allModel.removeFromParentNode()
        }
    }
    
    func setupPhysics() {
        if let scene = self.sceneView?.scene {
            scene.physicsWorld.contactDelegate = self
        }
    }
    
    func setupRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped(gesture:)))
        self.sceneView?.addGestureRecognizer(tap)
    }
    
    func tapped(gesture:UITapGestureRecognizer) {
        //find the nearest plane
        
        let tapPoint = gesture.location(in: self.sceneView!)
        let arHitResult = self.sceneView?.hitTest(tapPoint, types: .existingPlaneUsingExtent)
        if arHitResult!.count == 0 {
            return
        }
        self.insertCube(hitResult: arHitResult!.first!)
    }
    
    func insertCube(hitResult: ARHitTestResult) {
        let insertionYOffset:Float = 0.01
        let position = SCNVector3(hitResult.worldTransform.columns.3.x,
                                  hitResult.worldTransform.columns.3.y + insertionYOffset,
                                  hitResult.worldTransform.columns.3.z)
        if let allModel = self.allModel {
            allModel.removeFromParentNode()
        }
        
        self.allModel = AllModel(position: position)
        self.sceneView?.scene.rootNode.addChildNode(self.allModel!)
    }
    
    func setupScene() {
        self.sceneView = ARSCNView.init()
        self.arConfig = ARWorldTrackingSessionConfiguration()
        
        if let sceneView = self.sceneView,
            let arConfig = self.arConfig {
            
            let scene = SCNScene()
            
            sceneView.scene = scene
            sceneView.antialiasingMode = .multisampling4X
            
            arConfig.isLightEstimationEnabled = true
            arConfig.planeDetection = .horizontal
            
            sceneView.autoenablesDefaultLighting = true;
            sceneView.automaticallyUpdatesLighting = true;
            sceneView.debugOptions = [
//                ARSCNDebugOptions.showWorldOrigin
//                ARSCNDebugOptions.showFeaturePoints
                
            ]
            sceneView.delegate = self;
            
            sceneView.session.run(arConfig);
            
        }
        self.planes = [:]
    }
}

extension CameraViewController: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        let plane = Plane(anchor: anchor as! ARPlaneAnchor, isHidden: false, with: Plane.currentMaterial())
        self.planes![anchor.identifier.uuidString] = plane
        node.addChildNode(plane!)
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if let plane = self.planes![anchor.identifier.uuidString] {
            plane.update(anchor as! ARPlaneAnchor)
        }
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRemove node: SCNNode, for anchor: ARAnchor) {
        self.planes![anchor.identifier.uuidString] = nil
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        switch camera.trackingState {
        case .notAvailable:
            self.statusDot?.backgroundColor = UIColor.red
        case .limited:
            self.statusDot?.backgroundColor = UIColor.orange
        case .normal:
            self.statusDot?.backgroundColor = UIColor.green
        }
    }
}

extension CameraViewController: SCNPhysicsContactDelegate {
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact) {
        print("begin contact")
    }
    
    func physicsWorld(_ world: SCNPhysicsWorld, didUpdate contact: SCNPhysicsContact) {
        print("update contact")
    }
    
    func physicsWorld(_ world: SCNPhysicsWorld, didEnd contact: SCNPhysicsContact) {
        print("end contact")
    }
}
