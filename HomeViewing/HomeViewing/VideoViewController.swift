//
//  VideoViewController.swift
//  HomeViewing
//
//  Created by Patrick Cho on 8/5/17.
//  Copyright © 2017 TinyWhale. All rights reserved.
//

import UIKit

import AVKit
import AVFoundation

class VideoViewController: UIViewController {
    
    var moviePlayer: AVPlayer?
    var playButton: UIButton?
    var playing = false

    override func viewDidLoad() {
        super.viewDidLoad()
        generateVideo()
        self.playButton = UIButton()
        playButton?.backgroundColor = .red
        playButton?.setTitle("Play", for: .normal)
        playButton?.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(playButton!)
        let pvWidthConstraint = NSLayoutConstraint(item: playButton!, attribute: .width, relatedBy: .equal,
                                                   toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100)
        let pvHeightConstraint = NSLayoutConstraint(item: playButton!, attribute: .height, relatedBy: .equal,
                                                    toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 44)
        let pvXConstraint = NSLayoutConstraint(item: playButton!, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1, constant: -10)
        let pvYConstraint = NSLayoutConstraint(item: playButton!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -10)
        NSLayoutConstraint.activate([pvWidthConstraint, pvHeightConstraint, pvXConstraint, pvYConstraint])
        playButton?.addTarget(self, action: #selector(playVideo(sender:)), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.moviePlayer?.currentItem, queue: nil, using: { (_) in
            DispatchQueue.main.async {
                self.moviePlayer?.seek(to: kCMTimeZero)
                self.moviePlayer?.play()
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func generateVideo() {
        guard let path = Bundle.main.path(forResource: "video", ofType:"MOV") else {
            debugPrint("video.MOV not found")
            return
        }
        let videoURL = URL(fileURLWithPath: path)
        moviePlayer = AVPlayer(url: videoURL)
        let playerLayer = AVPlayerLayer(player: moviePlayer)
        playerLayer.frame = CGRect(x: 20, y: 0, width: self.view.bounds.size.width-40, height: self.view.bounds.size.height-40)
        self.view.layer.addSublayer(playerLayer)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (playing) {
            playVideo(sender: self)
        }
    }
    
    func playVideo(sender: AnyObject) {
        if playing {
            moviePlayer?.pause()
            playing = false
            playButton?.setTitle("Play", for: .normal)
        } else {
            moviePlayer?.play()
            try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            playing = true
            playButton?.setTitle("Pause", for: .normal)
        }
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }

}
